<?php
/**
 * File: FTPDownloaderTest.php
 *
 * @author mrx, Etor
 * 
 * @package DataImportFTPDownloader
 * @subpackage FTPDownloader
 * @version 1.0.4
 */

namespace DataImportXMLCatalogueItemExtractor;

use Core\InputValidate\InputValidate;
use Core\Tool\XML2Array;

/**
 * Class XMLCatalogueItemExtractor
 *
 * @author mrx, Etor
 *
 * @package: DataImportXMLCatalogueItemExtractor
 * @subpackage XMLCatalogueItemExtractor
 * @version: 1.0.4
 */
class XMLCatalogueItemExtractor
{
    /**
     * Required fields
     * @var array $config
     */
    private $configKeys = [
        'fileToProcess',
    ];


    /**
     * Extract an array of catalogueItems (information on one product)
     * from GS1 xml-data-source string.
     * @param array $config
     */
    public function extract(array $config = null )
    {

        if(empty($config)) {
            $config = file_get_contents($filepath);
            $config = (array) json_decode($config);
            $config = $config['testing'];
            $config = $config["DataImport"]["DirectoryProcessor"];
        }

        // Input validation
        InputValidate::config($config, $this->configKeys);



        $fileToProcess = $config['fileToProcess'];
        // \InputValidate::xmlfile($fileToProcess);

        // SimpleXMLElement
        $xmlstring = file_get_contents( $fileToProcess );

        $xml = simplexml_load_string($xmlstring);

        $fileNameArray = explode('/',$fileToProcess);
        $fileName = end($fileNameArray);
        if(!$xml)
        {
            $message = "Malformed XML: $fileToProcess" ;
            throw new \Exception($message);
        }
        $xpath = "//*[name()='catalogue_item_notification:catalogueItemNotification']/catalogueItem";
        $xpathTransId = "//*[name()='transactionIdentification']";
        $xpathDocsId = "//*[name()='documentCommandHeader']/documentCommandIdentification";
        $aSXECIs = $xml->xpath($xpath);
        $aSXECIsTrans = $xml->xpath($xpathTransId);
        $aSXECIsDocs = $xml->xpath($xpathDocsId);

        $aStringCIs = array();
        $aCIs = array();

        foreach($aSXECIs as $aSXECI) {
            $aStringCIs[] = $aSXECI->asXML();
        }
        // var_dump($aStringCIs);

        foreach($aStringCIs as $aStringCI)
        {
            set_error_handler( array( $this, 'HandleXmlError'));
            $aCI = XML2Array::createArray($aStringCI);
            $aCI['transactionIdentification'] = $aSXECIsTrans[0];
            $aCI['documentIdentificacion'] = $aSXECIsDocs[0];
            $aCI['insertTimestamp'] = time();
            $aCI['importFileName'] = $fileName;
            $aCIs[] = $aCI ;
            restore_error_handler();
        }

        return $aCIs;
    }

    public function HandleXmlError($errno, $errstr, $errfile, $errline)
    {
        if ($errno==E_WARNING && (substr_count($errstr,"DOMDocument::loadXML()")>0))
        {
            // throw new DOMException($errstr);
        }
        else
            return false;
    }

}
