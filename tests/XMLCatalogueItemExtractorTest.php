<?php
/**
 * File: tests/XMLCatalogueItemExtractorTest.php
 *
 * @author Francesc Roma Frigole <cesc@roma.cat>, mrx <silentstar@riseup.net>
 *
 * @package DataImportXMLCatalogueItemExtractor
 * @subpackage XMLCatalogueItemExtractor
 * @version 1.0.5
 *
 */

namespace DataImportXMLCatalogueItemExtractor;


/**
 * Class XMLCatalogueItemExtractorTest
 *
 * @package DataImportXMLCatalogueItemExtractor
 * @subpackage XMLCatalogueItemExtractor
 * @version 1.0.5
 */
class XMLCatalogueItemExtractorTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
    }

    public function tearDown()
    {
    }

    public function test()
    {
        $fileToProcess = __DIR__ . '/data/toProcess/testFile.xml';
        $config = array('fileToProcess' => $fileToProcess);

        $xcie = new \DataImportXMLCatalogueItemExtractor\XMLCatalogueItemExtractor();
        $r = $xcie->extract($config);

        // $r should be xml string
        // \InputValidate::xmlstring($r);

        $this->assertTrue(true);
    }
}
