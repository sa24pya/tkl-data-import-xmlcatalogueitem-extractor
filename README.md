# TKL data-import-xmlcatalogueitem-extractor
TKL data-import-xmlcatalogueitem-extractor
TKL data-import-xmlcatalogueitem-extractor provides xml-extrasction of catalogueItem nodes from GS1 data-source xml files.  
Support PSR-1, PSR-2, PSR-3, PSR-4.

![sa24 package repository](https://www.dream-destination-wedding.com/images/exotic2.jpg)

## Install

Via Composer

``` bash
$ composer require tkl/data-import-xmlcatalogueitem-extractor
```

## Usage

``` bash
$ php
$ 
```

## Testing

``` bash
$ phpunit
```

## Contributing

Please see [CONTRIBUTING](https://github.com/tkl/data-import-xmlcatalogueitem-extractor/blob/master/CONTRIBUTING.md) for details.

## Credits

- [Wilson Smith](https://github.com/wilsonsmith)
- [All Contributors](https://github.com/tkl/data-import-xmlcatalogueitem-extractor/contributors)

## License

The GNU General Public Licence version 3 (GPLv3). Please see [License File](LICENSE.md) for more information.
